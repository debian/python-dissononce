Source: dissononce
Section: python
Priority: optional
Maintainer: Josue Ortega <josue@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools,
 python3-cryptography (>= 2.5)
Standards-Version: 4.6.1
Homepage: https://github.com/tgalal/dissononce
Vcs-Browser: https://salsa.debian.org/debian/python-dissononce
Vcs-Git: https://salsa.debian.org/debian/python-dissononce.git

Package: python3-dissononce
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends},
 python3-transitions,
 python3-cryptography (>= 2.5)
Description: Python implementation for Noise Protocol Framework
 Dissononce is a Python implementation for Noise Protocol Framework. A main goal
 of this project is to provide a simple, easy to read and understand practical
 reference for Noise enthusiasts, implementers and users.
 Therefore this project attempts to stick to the following guidelines:
  - Syntax that resembles as closely as possible definitions and pseudo code
    mentioned in Noise Specs.
  - As minimal Python "magic" as possible (explicit is better than implicit).
  - Code that is simple, easy to read, follow and understand.
  - Flexibility to easily adopt future changes to Noise specifications.
  - Deviations from Noise Specs
    (additions, opinionated specs and API changes..etc) are  isolated from
    original implementation/API and are optional to use.
  - Deviations from Noise Specs do not influence adjustments to original
    implementation/API that conflict with Noise Specs.
